package com.example.demo.dao;

import com.example.demo.entity.Department;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class DepartmentRepositoryTest {

    @Resource
    private DepartmentRepository departmentRepository;

    @Test
    public void findAll() {
        List<Department> departments = departmentRepository.findAll();
        departments.forEach(System.out::println);
    }

    @Test
    public void update(){
        List<Department> departments = departmentRepository.findAll();
        for(Department department : departments){
            if ("研发部".equals(department.getName())){
                department.setName("R&D");
            } else if ("销售部".equals(department.getName())) {
                department.setName("Sell");
            }else if ("人事部".equals(department.getName())) {
                department.setName("Personnel");
            }else if ("财务部".equals(department.getName())) {
                department.setName("Financial");
            }else if ("运营部".equals(department.getName())) {
                department.setName("Operation");
            }
        }
        departmentRepository.saveAll(departments);
    }

}