package com.example.demo.dao;

import com.example.demo.entity.Project;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import jakarta.annotation.Resource;
import java.util.List;
/**
 * 单元测试
 * <pre>
 *     测试JPA 增删改查
 * </pre>
 */
@SpringBootTest
public class ProjectRepositoryTest {

    @Resource
    private ProjectRepository projectRepository;


    @Test
    public void findById() {
        projectRepository.findById(10L).ifPresent(System.err::println);
    }

    @Test
    public void findAll(){
        List<Project> projects = projectRepository.findAll();
        System.err.println(projects.size());
    }

    @Test
    public void save(){
        Project project = new Project();
        project.setId(11L);
        project.setName("测试项目");
        projectRepository.saveAndFlush(project);
        System.err.println(project.getId());
    }

    @Test
    public void update(){
        projectRepository.findById(11L).ifPresent(v -> {
            System.out.println(v.getName());
            v.setName(v.getName() + "1");
            projectRepository.saveAndFlush(v);
            System.err.println(v.getName());
        });
    }

    @Test
    public void delete(){
        projectRepository.deleteById(11L);
    }

}