package com.example.demo.controller.rest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
/**
 * 单元测试
 * <pre>
 *     测试 Rest 接口
 * </pre>
 */
@SpringBootTest
@AutoConfigureMockMvc
public class ProjectControllerTest {

    @Autowired
    private MockMvc mockMvc;

    /*获取列表*/
    @Test
    public void list() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/api/project"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    /*获取详情*/
    @Test
    public void get() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/api/project/10"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    /*保存*/
    @Test
    public void save() throws Exception{
        String jsonStr = "{\"name\":\"Apache Doris\"}";
        mockMvc.perform(MockMvcRequestBuilders.post("/api/project").contentType(MediaType.APPLICATION_JSON).content(jsonStr))
                .andExpect(status().isOk())
                .andDo(print());
    }

    /*更新*/
    @Test
    public void update() throws Exception{
        String jsonStr = "{\"name\":\"Apache Doris Cp\",\"id\":15}";
        mockMvc.perform(MockMvcRequestBuilders.put("/api/project").contentType(MediaType.APPLICATION_JSON).content(jsonStr))
                .andExpect(status().isOk())
                .andDo(print());
    }

    /*删除*/
    @Test
    public void delete()throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/project/15"))
                .andExpect(status().isOk())
                .andDo(print());
    }
}