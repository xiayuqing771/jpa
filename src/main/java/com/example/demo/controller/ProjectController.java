package com.example.demo.controller;

import com.example.demo.dao.ProjectRepository;
import com.example.demo.entity.Project;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import jakarta.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/project")
public class ProjectController extends BaseController {

    @Resource
    private ProjectRepository projectRepository;

    @GetMapping("/page")
    public String page(Model model) {
        List<Project> projects = projectRepository.findAll();
        model.addAttribute("projects", projects);
        return "project/page";
    }

}
