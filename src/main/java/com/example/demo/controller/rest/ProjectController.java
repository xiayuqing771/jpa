package com.example.demo.controller.rest;

import com.example.demo.controller.BaseController;
import com.example.demo.dao.ProjectRepository;
import com.example.demo.entity.Project;
import org.springframework.web.bind.annotation.*;

import jakarta.annotation.Resource;
import java.util.Map;
import java.util.Objects;

@RestController("apiProjectController")
@RequestMapping("/api/project")
public class ProjectController extends BaseController {

    @Resource
    private ProjectRepository projectRepository;

    @GetMapping
    Map<String, Object> list() {
        return success(projectRepository.findAll());
    }

    @GetMapping("/{id}")
    Map<String, Object> get(@PathVariable("id") Long id) {
        return success(projectRepository.findById(id).orElse(null));
    }

    @PostMapping
    Map<String, Object> save(@RequestBody Project project) {
        project.setId(null);
        projectRepository.save(project);
        return success(project.getId());
    }

    @PutMapping
    Map<String, Object> update(@RequestBody Project project) {
        boolean success = Objects.nonNull(project.getId()) && projectRepository.existsById(project.getId());
        if (success) {
            projectRepository.save(project);
        }
        return success(success);
    }

    @DeleteMapping("/{id}")
    Map<String, Object> delete(@PathVariable Long id) {
        boolean success = projectRepository.existsById(id);
        if (success) {
            projectRepository.deleteById(id);
        }
        return success(success);
    }
}
