package com.example.demo.controller;

import com.example.demo.dao.DepartmentRepository;
import com.example.demo.entity.Department;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import jakarta.annotation.Resource;
import java.util.List;


@Controller
@RequestMapping("/department")
public class DepartmentController extends BaseController{

    @Resource
    private DepartmentRepository departmentRepository;

    @GetMapping("/page")
    String page(Model model){
        List<Department> departments = departmentRepository.findAll();
        model.addAttribute("departments",departments );
        return "/department/page";
    }
}
