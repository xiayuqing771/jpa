package com.example.demo.controller;

import com.example.demo.dao.UserRepository;
import com.example.demo.entity.User;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Objects;

@Controller
@RequestMapping
public class IndexController extends BaseController {

    @Resource
    private UserRepository userRepository;

    @GetMapping("/")
    String toLogin() {
        return "login";
    }


    @GetMapping("/index")
    String toIndex() {
        return "index";
    }

    @GetMapping("/welcome")
    String toWelcome() {
        return "welcome";
    }


    @PostMapping("/login")
    @ResponseBody
    Map<String, Object> login(@RequestBody User user) {
        if (StringUtils.isEmpty(user.getUsername())) {
            return fail("The username must not be empty");
        }
        if (StringUtils.isEmpty(user.getPassword())) {
            return fail("The password must not be empty");
        }
        User entity = userRepository.findByUsernameAndPassword(user.getUsername(), user.getPassword());
        if (Objects.isNull(entity)) {
            return fail("The username or password is incorrect");
        }
        storeSessionUser(entity);
        return success(entity.getUserType());
    }

    @GetMapping("/logout")
    String logout() {
        removeSessionUser();
        return "redirect:/";
    }

}
