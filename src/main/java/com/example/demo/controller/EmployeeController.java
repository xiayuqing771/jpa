package com.example.demo.controller;

import com.example.demo.dao.DepartmentRepository;
import com.example.demo.dao.EmployeeRepository;
import com.example.demo.dao.ProjectRepository;
import com.example.demo.dao.UserRepository;
import com.example.demo.entity.Employee;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import jakarta.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;


@Controller
@RequestMapping("/employee")
public class EmployeeController extends BaseController {

    @Resource
    private EmployeeRepository employeeRepository;
    @Resource
    private DepartmentRepository departmentRepository;
    @Resource
    private ProjectRepository projectRepository;
    @Resource
    private UserRepository userRepository;

    @GetMapping("/page")
    String page(Model model,@RequestParam(required = false) String name) {
        List<Employee> employees = StringUtils.hasText(name) ? employeeRepository.findByNameLike("%" + name + "%") : employeeRepository.findAll();
        model.addAttribute("employees", employees);
        model.addAttribute("name", name);
        return "employee/page";
    }

    @GetMapping("/toAddOrEdit")
    String toAddOrEdit(@ModelAttribute Employee employee, Model model) {
        boolean isAdd = true;
        if(Objects.nonNull(employee.getId())){
            Optional<Employee> employeeOptional = employeeRepository.findById(employee.getId());
            if(employeeOptional.isPresent()){
                isAdd = false;
                employee = employeeOptional.get();
            }
        }
        model.addAttribute("isAdd", isAdd);
        model.addAttribute("bean",employee);
        model.addAttribute("departments", departmentRepository.findAll());
        model.addAttribute("projects", projectRepository.findAll());
        return "employee/edit";
    }

    @PostMapping
    @ResponseBody
    Map<String,Object> save(@ModelAttribute Employee employee) {
        if(Objects.isNull(employee.getId()) && userRepository.countByUsername(employee.getUser().getUsername()) > 0){
            return fail("Login Username Has Bean Register");
        }
        employeeRepository.save(employee);
        return success(true);
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    Map<String,Object> delete(@PathVariable Long id) {
        employeeRepository.deleteById(id);
        return success(true);
    }
}
