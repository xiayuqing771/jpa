package com.example.demo.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

/**
 * 用户表
 */
@Entity
@Table(name = "t_user")
public class User extends BaseEntity {

    @Column(name = "username",nullable = false,length = 32)
    private String username;

    @Column(name = "password",nullable = false,length = 64)
    private String password;

    @Column(name = "user_type",nullable = false)
    private Integer userType;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }
}
