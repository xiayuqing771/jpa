package com.example.demo.entity;

import jakarta.persistence.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "t_employee")
public class Employee extends BaseEntity {

    /**
     * 员工姓名
     */
    @Column(name = "name", nullable = false, length = 32)
    private String name;

    /**
     * 所属部门
     */
    @ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;

    /**
     * 用户
     */
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", unique = true)
    private User user;

    @ManyToMany
    @JoinTable(
            name = "t_project_employee",
            joinColumns = @JoinColumn(name = "employee_id"),
            inverseJoinColumns = @JoinColumn(name = "project_id")
    )
    private Set<Project> projects;

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getDepartmentId() {
        return Objects.isNull(department) ? null : department.getId();
    }

    public void setDepartmentId(Long departmentId) {
        if (Objects.isNull(departmentId)) {
            return;
        }
        if (Objects.isNull(department)) {
            department = new Department();
        }
        department.setId(departmentId);
    }

    public Long getUserId() {
        return Objects.isNull(user) ? null : user.getId();
    }

    public void setUserId(Long userId) {
        if (Objects.isNull(userId)) {
            return;
        }
        if (Objects.isNull(user)) {
            user = new User();
        }
        user.setId(userId);
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }

    public void setProjectIds(Set<Long> projectIds) {
        if (Objects.isNull(projectIds) || projectIds.isEmpty()) {
            return;
        }
        this.projects = new HashSet<>();
        for (Long projectId : projectIds) {
            Project project = new Project();
            project.setId(projectId);
            this.projects.add(project);
        }
    }

    public Set<Long> getProjectIds() {
        return projects.isEmpty() ? Collections.emptySet() : projects.stream().map(Project::getId).collect(Collectors.toSet());
    }

    @Override
    public String toString() {
        return super.toString();
    }
}

