/*
 MySQL Data Transfer
 Source Host: localhost
 Source Database: hsqldb
 Target Host: localhost
 Target Database: hsqldb
 */
DROP TABLE IF EXISTS t_project_employee;
DROP TABLE IF EXISTS t_employee;
DROP TABLE IF EXISTS t_user;
DROP TABLE IF EXISTS t_project;
DROP TABLE IF EXISTS t_department;

CREATE TABLE t_user
(
    id          INT      NOT NULL AUTO_INCREMENT COMMENT '用户ID',
    username    VARCHAR(32)      NOT NULL COMMENT '用户名',
    password    VARCHAR(64)      NOT NULL COMMENT '用户密码',
    user_type   TINYINT  NOT NULL DEFAULT 0 COMMENT '用户类型：0-员工，1-管理员',
    PRIMARY KEY (id)
);

INSERT INTO t_user(id,username, password,user_type) VALUES
(1000,'admin','123456',1),
(1001,'zhangsan','123456',0),
(1002,'lisi','123456',0);
-- ----------------------------
-- Table structure for department
-- ----------------------------
CREATE TABLE `t_department`
(
    `id`   INT  NOT NULL AUTO_INCREMENT COMMENT '主键：部门ID',
    `name` varchar(32) DEFAULT NULL COMMENT '部门名称',
    PRIMARY KEY (`id`)
);

INSERT INTO t_department(id,name)
VALUES
    (1,'研发部'),
    (2,'销售部'),
    (3,'人事部'),
    (4,'财务部'),
    (5,'运营部');

-- ----------------------------
-- Table structure for employee
-- ----------------------------
CREATE TABLE `t_employee`
(
    `id`            INT  NOT NULL AUTO_INCREMENT COMMENT '注解：员工ID',
    user_id         INT  NOT NULL COMMENT '员工用户ID',
    `name`          varchar(32)  DEFAULT NULL COMMENT '员工姓名',
    `department_id` INT  DEFAULT NULL COMMENT '员工归属部门',
    PRIMARY KEY (`id`),
    FOREIGN KEY (department_id) REFERENCES t_department (id),
    FOREIGN KEY (user_id) REFERENCES t_user (id)
);

INSERT INTO t_employee(id,user_id, name, department_id)
VALUES
    (1,1001,'张三',1) ,
    (2,1002,'李四',1) ;


-- ----------------------------
-- Table structure for project
-- ----------------------------
CREATE TABLE `t_project`
(
    `id`   INT  NOT NULL AUTO_INCREMENT,
    `name` varchar(32) DEFAULT NULL,
    PRIMARY KEY (`id`)
);

INSERT INTO t_project(id, name)
VALUES
    (8,'ShowDoc'),
    (9,'OpenCV'),
    (10,'OpenAI');

CREATE TABLE t_project_employee
(
    project_id  INT  NOT NULL,
    employee_id INT  NOT NULL,
    PRIMARY KEY (project_id, employee_id),
    FOREIGN KEY (project_id) REFERENCES t_project (id),
    FOREIGN KEY (employee_id) REFERENCES t_employee (id)
);

INSERT INTO t_project_employee
VALUES
    (10,1),
    (10,2);
